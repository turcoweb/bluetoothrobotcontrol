package com.android.BluetoothRobotControl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Robot {

	private static final byte CERO = 127;
	private static final byte MAX = (byte) 255;
	private static final byte MIN = 0;

	private byte izquierda = CERO, derecha = CERO;

	public void incIzquierda() {

		if (izquierda == MAX)
			return;

		izquierda++;
	}

	public void decIzquierda() {

		if (izquierda == MIN)
			return;

		izquierda--;
	}

	public void incDerecha() {

		if (derecha == MAX)
			return;

		derecha++;
	}

	public void decDerecha() {

		if (derecha == MIN)
			return;

		derecha--;
	}

	public void stop() {
		derecha = izquierda = CERO;
	}

	public void write(OutputStream out) throws IOException {
		out.write(izquierda);
		out.write(derecha);
	}

	public void read(InputStream in) throws IOException {

		int i = in.read();

		if (i < 0)
			throw new IOException();

		izquierda = (byte) i;

		int d = in.read();

		if (d < 0)
			throw new IOException();

		derecha = (byte) d;
	}

	@Override
	public String toString() {
		return "[" + izquierda + ", " + derecha + "]";
	}
}
