package com.android.BluetoothRobotControl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class Robots {

	private final Robot[] robots = new Robot[5];

	public void incIzquierda(int i) {
		robots[i % robots.length].incIzquierda();
	}

	public void decIzquierda(int i) {
		robots[i % robots.length].decIzquierda();
	}

	public void incDerecha(int i) {
		robots[i % robots.length].incDerecha();
	}

	public void decDerecha(int i) {
		robots[i % robots.length].decDerecha();
	}

	public void stop() {
		for (Robot robot : robots)
			robot.stop();
	}

	public void write(OutputStream out) throws IOException {
		out.write(0);
		for (Robot robot : robots)
			robot.write(out);
		out.write(0);
	}

	public void read(InputStream in) throws IOException {
		in.read(); // 0. Se descarta.
		for (Robot robot : robots)
			robot.read(in);
		in.read(); // 0. Se descarta.
	}
	
	@Override
	public String toString() {
		return Arrays.toString(robots);
	}
}
